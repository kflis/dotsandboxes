// Dox.java - CS 171, Winter, 2013

import java.util.*;

public class Dox
{
    public static int numRowsP, numColsP;
    public static String whoseTurnP;
    public static String linesP;
    public static String boxesP;
    public static int miniMaxPliesP;
    public static boolean useAlphaBetaP;
    public static int evaluationFunctionP;
    
    public static final boolean INTERACTIVE_PLAY = true;
    public static final boolean USE_TEST_ARGS = true;

    public static void main(String[] args)
    {
		// Parse and display command line arguments.  Samples:
		// 3x3 A XXXX..X.XXX. AB.. 3 Y 1
		// 5x5 B XXX....XXXX.XXXXXXXX.XXXX.XX.XXXX.XX.X.X ........AAA.AAAB 3 Y 1
		// 6x3 A X....XX.XXX.X..XX...XXX.... ......A.A. 3 Y 1
    	String testArgs[] = {"3x3", "A", "XXXX..X.XXX.", "AB..", "3", "Y", "1"};
    	String testArgs2[] = {"5x5", "B", "XXX....XXXX.XXXXXXXX.XXXX.XX.XXXX.XX.X.X", "........AAA.AAAB", "4", "Y", "1"};
    	String testArgs3[] = {"6x3", "A", "X....XX.XXX.X..XX...XXX....", "......A.A.", "3", "N", "2"};
    	String testArgs4[] = {"5x5", "B", "........................................", "................", "4", "Y", "1"};
        checkArguments(USE_TEST_ARGS? testArgs2: args);
        System.out.println("  rows and cols: " + numRowsP + " x " + numColsP);
        System.out.println("     whose Turn: " + whoseTurnP);
        System.out.println("          lines: " + linesP);
        System.out.println("          boxes: " + boxesP);
        System.out.println("  minimax plies: " + miniMaxPliesP);
        System.out.println(" use alpha-beta: " + useAlphaBetaP);
        System.out.println("evaluation func: " + evaluationFunctionP);
        System.out.println("");

		int move;
        Dox dox = new Dox(numRowsP, numColsP, linesP, boxesP,
        				miniMaxPliesP, useAlphaBetaP, evaluationFunctionP);


		// With this block of code, the computer chooses a single move and
		// that's it.  This matches the assignment.
        if (!INTERACTIVE_PLAY) {
			System.out.println(dox.board + "\n");
			long startTime = System.nanoTime();
	        move = dox.chooseMove(whoseTurnP);
	        long elapsedTime = System.nanoTime() - startTime;
	        System.out.println("The computer's move is " + move);
	        System.out.format("Elapsed time: %,d%n", elapsedTime);
	        dox.board = dox.board.applyMove(move, whoseTurnP);
			System.out.println(dox.board);
			System.out.println("Computer gets another move: " + dox.board.anotherMove);
        }


		//
        // With this block of code, the computer and human play interactively.
        else {
	   		Scanner scanner = new Scanner(System.in);
	   		boolean isComputersTurn = true;   // the computer always starts
	   		String computersLetter = whoseTurnP;
	   		String humansLetter = whoseTurnP.equals("A")? "B" : "A";
	        while (!dox.board.gameIsOver())
	        {
				System.out.println(dox.board + "\n");
				if (isComputersTurn)
				{
					move = dox.chooseMove(computersLetter);
					System.out.println("The computer's move is " + move);
	        		dox.board = dox.board.applyMove(move, computersLetter);
					if (dox.board.anotherMove)
						System.out.println("The computer takes a square and gets another move.");
					else
						isComputersTurn = false;
				}
				else
				{
					System.out.println("What's your move?");
					System.out.println(dox.board.validMoves());
					move = scanner.nextInt();
					DoxBoard newBoard = dox.board.applyMove(move, humansLetter);
					dox.board = newBoard;
					if (dox.board.anotherMove)
						System.out.println("The human takes a square and gets another move.");
					else
						isComputersTurn = true;
				}
			}
			System.out.println("Final position:\n" + dox.board);
			System.out.println("The score is " + dox.board.numBoxesOwnedBy("A") +
					" points for A, and " + dox.board.numBoxesOwnedBy("B") +
					" points for B.");
        }

	}

	private int numRows;
	private int numCols;
	private int levels;
	private boolean useAB;
	private int evalFunc;
	private DoxBoard board;
	private String maximum, minimum; //added to keep track of max and min

	public Dox(int r, int c, String lines, String boxes,
				int levels, boolean useAB, int evalFunc)
	{
		numRows = r;
		numCols = c;
		this.levels = levels;
		this.useAB = useAB;
		this.evalFunc = evalFunc;
		board = new DoxBoard(numRows, numCols, lines, boxes);
	}
	
	/*
	 * My custom chooseMove method which implements minimax and alpha-beta pruning--if specified
	 *
	 * chooseMove functions like getMax, except it stores a move associated with an evaluation,
	 * rather than just the evaluation, and returns the best move
	 */
	public int chooseMove(String whoseTurn) {
		
		
		int alpha = Integer.MIN_VALUE;
		int beta = Integer.MAX_VALUE;
		
		maximum = whoseTurn; //max
		minimum = swapTurn(whoseTurn); //min
		
		List<Integer> bestMoves = new ArrayList<Integer>(); //holds the best moves max can make
		Random rand = new Random(); //used to randomly pick a best move
		
		for (Integer move: board.validMoves()) {
			
			DoxBoard tempBoard = board.applyMove(move, maximum);
			
			//after applying each move, check if max gets another. if max gets another move, call getMax instead of getMin			
			int eval = tempBoard.anotherMove ? getMax(levels-1, tempBoard, alpha, beta) : getMin(levels-1, tempBoard, alpha, beta);
			
			
			if (eval > alpha) { //adjust alpha if the evaluation returned by a move is bigger than alpha then add the move to a new bestMoves list
				alpha = eval;
				bestMoves.clear();
				bestMoves.add(move);
			}
			else
				if (eval == alpha) //if the evaluation returned by a move equals alpha, add it to bestMoves
					bestMoves.add(move);
		}
		return bestMoves.get((rand.nextInt(bestMoves.size()))); //randomly return a best move
	}	
	
	/*
	 * getMax implements max's moves for minimax
	 * 
	 * the method calls getMax (if max gets another move) or getMin until there are no more levels left to explore
	 * 
	 * if a DoxBoard state shows that the game is over or if pliesLeft is less than or equal to 0, an evaluation of
	 * the current state is returned using the specified evaluation function (1 = simple, 2 or not 1 = custom)
	 */
	public int getMax(int pliesLeft, DoxBoard db, Integer alpha, Integer beta) {

		if (db.gameIsOver() || pliesLeft <= 0)
			return evalFunc == 1 ? simpleBoardEval(db, maximum) : customBoardEval(db, maximum);
		
		int bestEval = Integer.MIN_VALUE; //bestEval represents the alpha of getMax
		
		boolean prune = false; //used check if a move should be pruned
		
		for (Integer move : db.validMoves()){		
			
			DoxBoard tempBoard = db.applyMove(move, maximum);
			
			/*
			 * After applying a move, if max gets another move, always evaluate it
			 * 
			 * if max does not get another move and the prune flag is enabled, prune the move
			 * 
			 * note: pruning will never happen is useAB is false, since useAB must be true in order for
			 * prune to be true
			 */
			if (tempBoard.anotherMove || !prune) {
				
				//after applying each move, check if max gets another. if max gets another move, call getMax instead of getMin
				int max = tempBoard.anotherMove ? getMax(pliesLeft-1, tempBoard, bestEval, beta) : getMin(pliesLeft-1, tempBoard, bestEval, beta);
				
				if (max > bestEval)
					bestEval = max;
				
				if (useAB && beta <= bestEval) //if pruning is enabled and beta (the minimum upper bound) is less than equal to bestEval (alpha),
					prune = true;              //prune all moves that would call getMin
				
				
			}
		}
		return bestEval;
	}
	
	/*
	 * getMin implements mins's moves for minimax
	 * 
	 * the method calls getMin (if min gets another move) or getMax until there are no more levels left to explore
	 * 
	 * if a DoxBoard state shows that the game is over or if pliesLeft is less than or equal to 0, an evaluation of
	 * the current state is returned using the specified evaluation function (1 = simple, 2 or not 1 = custom)
	 */
	public int getMin(int pliesLeft, DoxBoard db, Integer alpha, Integer beta) {
		
		if (db.gameIsOver() || pliesLeft <= 0)
			return evalFunc == 1 ? simpleBoardEval(db, maximum) : customBoardEval(db, minimum);
		
		int bestEval = Integer.MAX_VALUE; //bestEval represents the beta of getMin
		
		boolean prune = false;//used check if a move should be pruned
		
		/*
		 * After applying a move, if min gets another move, always evaluate it
		 * 
		 * if min does not get another move and the prune flag is enabled, prune the move
		 * 
		 * note: pruning will never happen is useAB is false, since useAB must be true in order for
		 * prune to be true
		 */
		for (Integer move : db.validMoves()){
			
			DoxBoard tempBoard = db.applyMove(move, minimum);
			
			if (tempBoard.anotherMove || !prune) {
			
				//after applying each move, check if min gets another. if min gets another move, call getMin instead of getMax
				int min = tempBoard.anotherMove ? getMin(pliesLeft-1, tempBoard, alpha, bestEval) : getMax(pliesLeft-1, tempBoard, alpha, bestEval);
				
				if (min < bestEval)
					bestEval = min;
				
				if (useAB && bestEval <= alpha) //if pruning is enabled and bestEval (beta) is less than or equal to alpha (the maximum lower bound),
					prune = true;               //prune all moves that would call getMax
				
			}
			
			
		}
		return bestEval;
	}
	
	//method to make turn swaps easier to read
	public String swapTurn(String whoseTurn) {
		return whoseTurn.equals("A") ? "B" : "A";
	}

	// This simple approach returns the number of boxes max has
	// minus the number of boxes the opponent has.
	private int simpleBoardEval(DoxBoard b, String max)
	{
		String min = max.equals("A")? "B": "A";
		return b.numBoxesOwnedBy(max) - b.numBoxesOwnedBy(min);
	}
	
	
	/*
	 * My customBoardEval method first calculates the max's boxes minus min's boxes in the variable "difference"
	 * 
	 * Then if the game is over, returns the difference added to 99999 if max is winning,
	 * -99999 if min is winning, or returns just 0 if it's a tie. 
	 * 
	 * If the game isn't over, the variable maxChain holds the result of my getMaxChain() 
	 * method that I wrote in the DoxBoard class, which returns biggest chain of moves the
	 * player who moves next can perform in a single turn. 
	 * 
	 * If the it's max's turn for when the chain is calculated, the maxChain is added to difference and then returned
	 * otherwise, the maxChain is subtracted from difference then returned
	 */
	private int customBoardEval(DoxBoard b, String whoseTurn) {
		
		int difference = b.numBoxesOwnedBy(maximum) - b.numBoxesOwnedBy(minimum);
		
		if (b.gameIsOver())
			if (difference > 0)
				return 99999 + difference;
			else
				if (difference < 0)
					return -99999 + difference;
				else
					return 0;
		
		int maxChain = b.getMaxChain();
		
		return whoseTurn.equals(maximum) ? difference + maxChain: difference - maxChain;
	}


    // checkArguments parses the command line.
    private static void checkArguments(String[] args)
    {
		if (args.length < 6)
			printUsageAndExit("number of parameters");
		try
		{
			String[] parms = args[0].split("x");
			if (parms.length != 2)
				printUsageAndExit("first parm not in RxC format");
			numRowsP = Integer.decode(parms[0]).intValue();
			numColsP = Integer.decode(parms[1]).intValue();

			if (args[1].equals("A") || args[1].equals("B"))
			{
				whoseTurnP = args[1];
			}
			else
				printUsageAndExit("second parm should be A or B, found " +
								"<" + args[1] + ">");

			int numLinePlaces = numRowsP * (numColsP - 1) + numColsP * (numRowsP - 1);
			if (args[2].length() != numLinePlaces)
				printUsageAndExit("third parm should have length " + numLinePlaces +
								", found " + args[2].length());
			if (args[2].matches("\\A[X\\.]*\\z"))
				linesP = args[2];
			else
				printUsageAndExit("third parm must consist only of X and .");

			int numBoxes = (numRowsP - 1) * (numColsP - 1);
			if (args[3].length() != numBoxes)
				printUsageAndExit("fourth parm should have length " + numBoxes +
								", found " + args[3].length());
			if (args[3].matches("\\A[AB\\.]*\\z"))
				boxesP = args[3];
			else
				printUsageAndExit("fourth parm must consist only of A and B and .");

			miniMaxPliesP = Integer.decode(args[4]).intValue();

			if (args[5].equals("Y"))
				useAlphaBetaP = true;
			else if (args[5].equals("N"))
				useAlphaBetaP = false;
			else
				printUsageAndExit("sixth parm must be Y or N");

			if (args[6].equals("1"))
				evaluationFunctionP = 1;
			else if (args[6].equals("2"))
				evaluationFunctionP = 2;
			else
				printUsageAndExit("seventh parm must be 1 or 2");
		}
		catch (Exception e)
		{
			printUsageAndExit(e.toString());
		}
    }

    public static void printUsageAndExit(String message)
    {
		System.out.println(
			"Run Dox with 6 command line arguments.\n" +
			"  rows x cols - e.g. 4x5\n" +
			"  player to move - A or B\n" +
			"  already drawn lines - horizontal, then vertical, e.g. XX...XX....X\n" +
			"  box owners - left to right, top to bottom, e.g. A..B\n" +
			"  number of plies for minimax\n" +
			"  Y to use alpha-beta pruning, N otherwise.\n" +
			"\n" +
			"Your error message is: " + message);
		System.exit(0);
	}
}
